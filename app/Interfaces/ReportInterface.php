<?php

namespace App\Interfaces;

interface ReportInterface{

    public function getReport();
}
