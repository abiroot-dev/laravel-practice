<?php

namespace App\Services;

use App\Interfaces\ReportInterface;

class YearlyReportService implements ReportInterface{

    protected $year;


    public function __construct($year)
    {
        $this->year = $year;
    }


    public function getReport()
    {

        $userYear= (string)$this->year;
        $result ="Yearly report of the year " .$userYear;

        return $result;
    }
}
