<?php

namespace Modules\User\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProviders;
use Modules\User\Events\UserRegisteredEvent;
use Modules\User\Listeners\SendMailToUserListener;
use Modules\User\Listeners\SubscribeUserToNewsPaperListener;
use Spatie\EventSourcing\Facades\Projectionist;

class EventServiceProvider extends ServiceProviders
{

//    protected $listen=[
//        UserRegisteredEvent::class => [
//            SendMailToUserListener::class,
//            SubscribeUserToNewsPaperListener::class,
//        ],
//    ];

    public function boot()
    {
        //
    }
    public function register()
    {

        Projectionist::addProjectors([
            SendMailToUserListener::class,
            SubscribeUserToNewsPaperListener::class,
        ]);
    }
}
