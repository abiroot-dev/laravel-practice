<?php

namespace Modules\User\Events;

use Illuminate\Queue\SerializesModels;
use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class UserRegisteredEvent extends ShouldBeStored
{
    use SerializesModels;

    public $user;

    public $userID;

    public function __construct($user)
    {
        $this->user=$user;
        $this->userID=$user->id;
    }

}
