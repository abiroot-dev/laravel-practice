<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Http\Controllers\RoleController;
use Modules\User\Entities\UserModel;
use Modules\User\Events\UserRegisteredEvent;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function register(Request $request)
    {
        $name=$request->input('name');
        $password=$request->input('password');
        $role_id=$request->input('roleid');
        $register=new UserModel();
        $register->name=$name;
        $register->password=$password;
        $register->RoleId=$role_id;
        $register->save();

        $user=UserModel::where('name',$name)->first();

        event(new UserRegisteredEvent($user));

        //return redirect('/login');
    }

    public function login(Request $request)
    {
        $login=new UserModel();
        $login=UserModel::where('name',$request->input('name'))->where('password',$request->input('password'))->first();
        if($login!==null){

            echo '<script>' ;
            echo 'alert("Logged in! ")';
            echo '</script>';
            session(['Role'=>$login->RoleId]);
            return redirect('/home');
        }else{

            echo '<script>' ;
            echo 'alert("Wrong username or password")';
            echo '</script>';

            return redirect('/login');
        }
    }

    public function isAdmin($roleid)
    {
       $role=new RoleController();
       $getRoleName=$role->getRole($roleid);
       return $getRoleName;
    }

    public function getUsers(){
        return UserModel::all();
    }
}
