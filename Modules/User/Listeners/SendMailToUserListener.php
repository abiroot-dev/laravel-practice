<?php

namespace Modules\User\Listeners;

use Modules\User\Events\UserRegisteredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\EventSourcing\EventHandlers\Projectors\Projector;

class SendMailToUserListener extends Projector
{

    public function OnCreation(UserRegisteredEvent $event)
    {
        dump("Mail sent to" .$event->user->name);
    }
}
