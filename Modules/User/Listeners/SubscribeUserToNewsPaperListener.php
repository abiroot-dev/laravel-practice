<?php

namespace Modules\User\Listeners;

use Modules\User\Events\UserRegisteredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\EventSourcing\EventHandlers\Projectors\Projector;

class SubscribeUserToNewsPaperListener extends Projector
{

    public function onCreation(UserRegisteredEvent $event)
    {
        dump('User is now subscribed to NewsPaper');
    }
}
