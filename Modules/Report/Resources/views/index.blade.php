@extends('report::layouts.master')

@section('content')
    <h1>Hello World</h1>

    <p>
        This view is loaded from module: {!! config('report.name') !!}
    </p>

    <h2>This is an {{$result}}</h2>

    <form action="{{route('yearly.report')}}" method="post">
    {{csrf_field()}}
        <h3>Add a year:</h3>
        <input type="number" name="year">
        <input type="submit">

    </form>
@endsection
