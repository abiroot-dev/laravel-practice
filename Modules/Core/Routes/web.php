<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Modules\User\Http\Controllers\UserController;


Route::get('/', 'CoreController@index')->name('register');

Route::get('/login', function(){
    return view('core::login');
});

Route::get('/home', function(){
    return view('core::home');
});

Route::get('/admin', function(){
    //
})->middleware('role:Admin');

Route::get('/adminPage',function(){
    return view('core::admin');
});

Route::prefix('core')->group(function() {
    Route::post('/userRegister', [UserController::class,'register'])->name('register.user');

    Route::post('/userLogin', [UserController::class,'login'])->name('login.user');
});
