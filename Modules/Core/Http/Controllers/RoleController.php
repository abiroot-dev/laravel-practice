<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\User\Entities\Role;

class RoleController extends Controller
{
    public function returnRoles()
    {
        $role=new Role();
        $role=Role::all();
        return $role;
    }

    public function getRole($roleid){
        $role=new Role();
        $role=Role::where('id',$roleid)->first();
        return $role->role;
    }
}
