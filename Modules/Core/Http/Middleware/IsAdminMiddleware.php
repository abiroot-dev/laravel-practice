<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\User\Http\Controllers\UserController;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$role)
    {
        $roles=new UserController();

        if($roles->isAdmin(session('Role'))==$role){
            return redirect('adminPage');
        }
        return redirect()->route('register');
    }
}
