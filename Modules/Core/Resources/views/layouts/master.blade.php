<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module Core</title>
        <h1>-----NavBar-----</h1>
       {{-- Laravel Mix - CSS File --}}
       {{-- <link rel="stylesheet" href="{{ mix('css/core.css') }}"> --}}

    </head>
    <body>
        @yield('content')
        @yield('login')
        @yield('home')
        @yield('admin')
        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ mix('js/core.js') }}"></script> --}}
    </body>
    <footer>
        <h3>---Footer---</h3>
    </footer>
</html>
