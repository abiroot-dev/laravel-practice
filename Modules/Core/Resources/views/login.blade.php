@extends('core::layouts.master')

@section('login')
    <div>
        <h1>Login</h1>

        <form action="{{route('login.user')}}" method="post">
            {{csrf_field()}}
            <div>
                <label>Name:</label>
                <input type="text" name="name">
            </div>

            <div>
                <label>Password:</label>
                <input type="password" name="password">
            </div>

            <div>
                <input type="submit" value="Submit">
            </div>
        </form>
    </div>
@endsection
