@extends('core::layouts.master')

@section('content')
    <div>
        <h1>Register</h1>
        <form action="{{route('register.user')}}" method="post">
            {{csrf_field()}}
            <div>
                <label>Name:</label>
                <input type="text" name="name">
            </div>

            <div>
                <label>Password:</label>
                <input type="password" name="password">
            </div>

            <div>
                <label>Role:</label>
                <select name="roleid">
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->role}}</option>
                    @endforeach
                </select>
            </div>

            <div>
                <input type="submit" value="Submit">
            </div>
        </form>

        <div>
            <h1>Already have an account?</h1>
            <a href="login">Log in!</a>
        </div>
    </div>

    <div id="app">
        <router-link to="/testing">Check all users</router-link>
        <router-view></router-view>
    </div>

    <script src="{{asset('js/app.js')}}"></script>


@endsection
