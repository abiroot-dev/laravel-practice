require('./bootstrap');

window.Vue = require('vue').default;
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Test from './components/TestVue';

const routes = [
    {
        path: '/testing',
        component: Test
    }
];

const router = new VueRouter({routes});

// Vue.component('test-vue', require('./components/TestVue.vue').default);



const app = new Vue({
    el: '#app',
    router: router,
});
